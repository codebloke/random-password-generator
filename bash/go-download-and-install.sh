#!/bin/bash

MYPWD=`pwd`
SCRIPT=$(readlink -f $0)
SCRIPTPATH=`dirname $SCRIPT`
cd $SCRIPTPATH

URLROOT='https://storage.googleapis.com/golang/'
URLFILE='go1.9.linux-amd64.tar.gz'
URLFULL="$URLROOT$URLFILE"
DOWNLOADTRGT="/tmp/"
clear
echo "+++++++++++++++==+++++++++++++++"
echo "+ go download & install v. 1.9 +"
echo "+++++++++++++++++==+++++++++++++"
echo ""
echo "This would install $URLFILE "
WGETHELP=`wget --help >/dev/null 2>&1`
if [ "$?" != "0" ]
then
    echo "Error: wget is not installed... "
    exit 1
else
    echo "Do you want to continue? Y / N"
    echo ""
    read GOINSTALL
    if [ "$GOINSTALL" != "Y" ]
    then
        echo "Install wget manually and restart the process"
        cd "$MYPWD"
        exit 1
    else
        echo ""
        echo "  1. Downloading... $URLFULL"
        wget $URLFULL -q -P "$DOWNLOADTRGT"
        echo "  2. Unpacking..."
        cd $DOWNLOADTRGT
        sudo tar -xf "$URLFILE"
        sudo mv go /usr/local
        WHOAMI=`whoami`
        CONFIGTARGET="/home/$WHOAMI/.profile"
        CONFIGTARGETNEW="/home/$WHOAMI/.profile.new"
        echo "  3. Environment setup... please enter "
        echo "     your file if other than $CONFIGTARGET "
        echo "     otherwise [Enter]"
        echo ""
        read CONFIGFILE
        if [ "$CONFIGFILE" != "" ]
        then
            CONFIGTARGET="$CONFIGFILE"
            echo "     file: $CONFIGTARGET"
        else
            echo "     file: $CONFIGTARGET"
        fi

        # create a backup file
        CONFIGTARGETBAK="$CONFIGTARGET.bk"
        cat "$CONFIGTARGET" > "$CONFIGTARGETBAK"
        echo "     backup: $CONFIGTARGETBAK"

        # create new config
        cat "$CONFIGTARGET" > "$CONFIGTARGETNEW"

        # config punch in / out lines calculated (if exists)
        LINEIN=`sed -n '/# golang/=' $CONFIGTARGET`
        LINEOUT=`sed -n '/# \/golang/=' $CONFIGTARGET`

        # remove existing lines from the config
        if [ "$LINEIN" != "" ] && [ "$LINEOUT" != "" ]
        then
            echo "     lines: $LINEIN - $LINEOUT are removed... "
            sed -e "${LINEIN},${LINEOUT}d" $CONFIGTARGET > "$CONFIGTARGETNEW"
        fi

        GOINSTDATE=`date '+%Y-%m-%d %H:%M:%S'`
        echo "# golang - installed: $GOINSTDATE"            >> "$CONFIGTARGETNEW"
        echo ""                                             >> "$CONFIGTARGETNEW"
        echo "export GOROOT=/usr/local/go"                  >> "$CONFIGTARGETNEW"
        echo "export GOPATH=\$(pwd)"                        >> "$CONFIGTARGETNEW"
        echo "export PATH=\$GOPATH/bin:\$GOROOT/bin:\$PATH" >> "$CONFIGTARGETNEW"
        echo ""                                             >> "$CONFIGTARGETNEW"
        echo "# /golang"                                    >> "$CONFIGTARGETNEW"

        echo "     file: $CONFIGTARGETNEW"

        cat "$CONFIGTARGETNEW" > "$CONFIGTARGET"
        source "$CONFIGTARGET"
        echo ""
        GOVER=`go version`
        echo "     $GOVER"
        cd "$MYPWD"
        echo ""
        echo "DONE!"
        echo ""
        echo "+++++++++++++++++++++++++++++++++++++++++++++"
        echo "+ Caution: Make sure that your Environment  +"
        echo "+          configuration file is sourced    +"
        echo "+          anew. Best OPEN NEW SHELL WINDOW +"
        echo "+          or                               +"
        echo "+          $ source YOURCONFIG              +"
        echo "+++++++++++++++++++++++++++++++++++++++++++++"
        echo ""
        exit 0
    fi
fi