#!/bin/bash
clear
echo "++++++++++++++++++++++++++++++"
echo "+ go checker & installer     +"
echo "++++++++++++++++++++++++++++++"
echo ""
MYPWD=`pwd`
SCRIPT=$(readlink -f $0)
SCRIPTPATH=`dirname $SCRIPT`
cd $SCRIPTPATH

EXPECTED='Ubuntu'
SYSTEM=`./system.sh | grep $EXPECTED`
if [[ -z "${SYSTEM// }" ]]
then
    echo "System is not $EXPECTED"
    exit 1
fi

GOCHKVER=`go version >/dev/null 2>&1`

if [ "$?" != "0" ]
then
    echo "go is NOT installed... "
    echo "Do you want to continue and have script download/install it?  Y / N"
    echo ""
    read GOAHEAD
    if [ "$GOAHEAD" != "Y" ]
    then
        echo "OK. Please install go manually..."
        echo ""
        cd "$MYPWD"
        exit 1
    else
        ./go-download-and-install.sh
    fi
else
    echo "Installed..."
    go version
    cd "$MYPWD"
    exit 0;
fi
